﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ContosoUniversity.TagHelpers
{
    /// <summary>
    /// Tag helper using a naming convetion so the <email></email> tag will be targeted
    /// </summary>
    public class EmailTagHelper : TagHelper
    {
        //public override void Process(TagHelperContext context, TagHelperOutput output)
        //{
        //    output.TagName = "a"; // Replaces <email> with <a> tag
        //    output.Attributes.SetAttribute("href", "mailto:support@contoso.com");
        //    output.Content.SetContent("support@contoso.com");
        //}

        private const string EmailDomain = "contoso.com";
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";                                 // Replaces <email> with <a> tag
            var content = await output.GetChildContentAsync();
            var target = content.GetContent() + "@" + EmailDomain;
            output.Attributes.SetAttribute("href", "mailto:" + target);
            output.Content.SetContent(target);
        }
    }
}
