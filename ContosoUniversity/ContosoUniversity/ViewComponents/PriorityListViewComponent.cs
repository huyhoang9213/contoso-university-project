﻿using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContosoUniversity.ViewComponents
{
    public class PriorityListViewComponent : ViewComponent
    {
        private readonly SchoolContext _context;
        public PriorityListViewComponent(SchoolContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int maxPriority, bool isDone)
        {
            string myView = "Default";

            // If asking for all completed tasks, render with the "PVCCompleted" view
            if(isDone == true)
            {
                myView = "PVCCompleted";
            }
            var items = await GetItemAsync(maxPriority, isDone);
            return View(myView, items);
        }

        private Task<List<TodoItem>> GetItemAsync(int maxPriority, bool isDone)
        {
            return _context.TodoItems.Where(x => x.IsDone == isDone && x.Priority <= maxPriority).ToListAsync();
        }
    }
}
